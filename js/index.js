$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
      interval: 2500
    });
    $('#contacto').on('show.bs.modal', function (e){
      console.log('el modal contacto se está mostrando');
        $('*#contactoBtn').removeClass('btn-info');
        $('*#contactoBtn').addClass('btn-outline-danger');
        $('*#contactoBtn').prop('disabled', true);
    });
    $('#contacto').on('shown.bs.modal', function (e){
      console.log('el modal contanco se mostró');
    });
    $('#contacto').on('hide.bs.modal', function (e){
      console.log('el modal contacto se oculta');
    });
    $('#contacto').on('hidden.bs.modal', function (e){
      console.log('el modal contacto se ocultó');
        $('*#contactoBtn').prop('disabled', false);
        $('*#contactoBtn').removeClass('btn-outline-danger');
        $('*#contactoBtn').addClass('btn-info');

    });
  });